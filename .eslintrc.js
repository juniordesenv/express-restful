module.exports = {
  "extends": "standard",
  "globals": {
    "describe": true,
    "it": true,
    "expect": true,
    "request": true,
    "beforeEach": true,
    "td": true,
    "Joi": true,
    "joiAssert": true,
    "sinon": true,
  }
}