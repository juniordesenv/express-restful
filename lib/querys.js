'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var mongoose = require('mongoose');

exports.default = function (Model) {
  return {
    get: function get(req) {
      return this.prepareAggregate(req);
    },
    getOne: function getOne(req) {
      var _this = this;

      return new Promise(function (resolve, reject) {
        _this.prepareAggregate(req).then(function (data) {
          if (data.length === 0) {
            resolve(null);
          } else if (data.length === 1) {
            resolve(data[0]);
          } else reject(new Error('Mais de um item retornado na busca unica'));
        }).catch(reject);
      });
    },
    post: function post(req) {
      return Model.create(req.body);
    },
    put: function put(req) {
      var command = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '$set';

      return Model.findOneAndUpdate(req.params, _defineProperty({}, command, _extends({}, req.body, { changedAt: new Date() })), { runValidators: true });
    },
    delete: function _delete(req) {
      return Model.findOneAndUpdate(req.params, { $set: { disabledAt: new Date(), active: false } }, { runValidators: true });
    },
    prepareAggregate: function prepareAggregate(req) {
      var _this2 = this;

      var _req$query = req.query,
          limit = _req$query.limit,
          populate = _req$query.populate,
          sort = _req$query.sort,
          select = _req$query.select,
          page = _req$query.page;

      var query = req.query;
      if (query) ['limit', 'populate', 'sort', 'select', 'page'].map(function (i) {
        delete query[i];
      });
      var params = req.params;
      var subQuerys = {};
      var listPopulate = [];
      if (populate) {
        if (!(populate instanceof Array)) populate = populate.split(',');
        populate.map(function (field) {
          Object.keys(query).forEach(function (key) {
            if (key.includes(field) && key.includes('.')) {
              subQuerys[key] = query[key];
              delete query[key];
            }
          });
          _this2.prepareLookup(listPopulate, field);
        });
      }
      query = _extends({}, query, params);
      this.prepareQuery(query);
      query = [{
        $match: query
      }];

      listPopulate.forEach(function (populate) {
        return query.push(populate);
      });

      query.push({
        $match: subQuerys
      });

      if (sort) {
        var sortAux = {};
        sort.split(' ').forEach(function (item) {
          sortAux[item.replace('-', '')] = item.indexOf('-') > -1 ? -1 : 1;
        });
        query.push({
          $sort: sortAux
        });
      }

      if (select) {
        var project = {};
        if (!(select instanceof Array)) select = select.split(',');
        select.forEach(function (field) {
          project[field] = 1;
        });
        query.push({
          $project: project
        });
      }

      if (page) {
        query.push({
          $skip: Number(page * limit)
        });
      }
      if (limit) {
        query.push({
          $limit: Number(limit)
        });
      }

      if (process.env.debugQuery) console.log(JSON.stringify(query));

      return Model.aggregate(query);
    },
    prepareQuery: function prepareQuery(query) {
      Object.keys(query).forEach(function (key) {
        if (Model.schema.paths[key]) {
          if (Model.schema.paths[key].instance === 'Number') query[key] = Number(query[key]);
          if (Model.schema.paths[key].instance === 'Date' && query[key]) console.log('cria Renge');
          if (Model.schema.paths[key].instance === 'ObjectID' && query[key]) query[key] = mongoose.Types.ObjectId(query[key]);
          if (Model.schema.paths[key].instance === 'Boolean') query[key] = query[key] === 'true';
        }
      });
    },
    prepareLookup: function prepareLookup(list, field) {
      var ref = void 0;
      var type = void 0;
      var unwind = false;
      var firstField = void 0;
      var split = field.split('.');
      if (field.indexOf('.') > -1 && Model.schema.paths[split[0]]) {
        firstField = split[0];
        var refChild = Model.schema.paths[split[0]].options.ref;
        if (refChild) {
          ref = mongoose.model(refChild).schema.paths[split[1]].options.ref.toLowerCase();
          type = mongoose.model(refChild).schema.paths[split[1]].instance;
        } else {
          var aux = Model.schema.paths[split[0]].schema.paths[split[1]];
          ref = aux.options.ref.toLowerCase();
          type = aux.instance;
          unwind = true;
          list.push({
            $unwind: { path: '$' + split[0], preserveNullAndEmptyArrays: true }
          });
        }
      }
      ref = !ref ? Model.schema.paths[field].options.ref.toLowerCase() : ref;
      type = !type ? Model.schema.paths[field].instance : type;
      list.push({
        $lookup: {
          from: ref,
          localField: field,
          foreignField: '_id',
          as: field
        }
      });
      if (type === 'ObjectID') {
        list.push({
          $unwind: {
            path: '$' + field,
            preserveNullAndEmptyArrays: true
          }
        });
      }
      if (unwind) {
        var group = {};
        Object.keys(Model.schema.paths).map(function (k) {
          if (k !== '_id') {
            if (k.includes('.')) k = k.substring(0, k.indexOf('.'));
            group[k] = { $first: '$' + k };
          }
        });
        // group[firstField] = {$push: `$${firstField}`}
        group[firstField] = { $push: {
            $cond: {
              if: '$' + firstField + '._id',
              then: '$' + firstField,
              else: null
            } }
        };
        list.push({
          $group: _extends({
            '_id': '$_id'
          }, group)
        });
        var project = {};
        Object.keys(Model.schema.paths).map(function (k) {
          project[k] = 1;
        });
        project[firstField] = {
          $filter: {
            input: '$' + firstField,
            as: 'itemGroup',
            cond: { $and: ['$$itemGroup._id'] }
          }
        };
        list.push({
          $project: _extends({
            _id: 1
          }, project)
        });
      }
    }
  };
};