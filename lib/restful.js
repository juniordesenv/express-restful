'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _querys = require('./querys');

var _querys2 = _interopRequireDefault(_querys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var methods = function methods(Model, methodsBefore, methodsAfter, methodsCustom, routeId, middleware) {
  var querysModel = (0, _querys2.default)(Model);
  var custom = { querysModel: querysModel, methodsAfter: methodsAfter, methodsBefore: methodsBefore, methodsCustom: methodsCustom, middleware: middleware, routeId: routeId };
  return {
    get: function get(req, res, next) {
      executeCompletProcess('get', req, res, next, custom);
    },
    getOne: function getOne(req, res, next) {
      executeCompletProcess('getOne', req, res, next, custom);
    },
    post: function post(req, res, next) {
      executeCompletProcess('post', req, res, next, custom);
    },
    put: function put(req, res, next) {
      executeCompletProcess('put', req, res, next, custom);
    },
    delete: function _delete(req, res, next) {
      executeCompletProcess('delete', req, res, next, custom);
    }
  };
};

var executeCompletProcess = async function executeCompletProcess(method, req, res, next, custom) {
  var methodsBefore = custom.methodsBefore,
      methodsCustom = custom.methodsCustom,
      middleware = custom.middleware,
      routeId = custom.routeId;

  if (middleware) {
    await new Promise(function (resolve) {
      middleware(req, res, resolve, routeId);
    });
  }
  if (methodsCustom[method]) return methodsCustom[method](req, res, next);
  if (methodsBefore[method]) {
    return methodsBefore[method](req, res, function () {
      executeQuery(method, req, res, custom);
    });
  }
  executeQuery(method, req, res, custom);
};

var executeQuery = function executeQuery(method, req, res, custom) {
  var querysModel = custom.querysModel,
      methodsAfter = custom.methodsAfter;

  var after = methodsAfter[method];
  querysModel[method](req).then(function (data) {
    if (!after) return res.send(data);
    res.locals.data = data;
    after(req, res, function () {
      res.send(res.locals.data);
    });
  }).catch(function (err) {
    console.log(err);
    var errors = err.errors || [{ message: err.message }];
    if (!Array.isArray(errors)) {
      errors = Object.keys(errors).map(function (i) {
        return errors[i];
      });
    }
    errors = errors.map(function (e) {
      return e.message;
    });
    if (!after) return res.status(422).send({ errors: errors });
    res.locals.errors = errors;
    after(req, res, function () {
      res.status(422).send({ errors: errors });
    });
  });
};

exports.default = function () {
  return {
    methodsBefore: {},
    methodsAfter: {},
    methodsCustom: {},
    extraRoutes: [],
    modelSchema: null,
    modelName: null,
    routeId: 0,
    methods: ['get', 'getOne', 'post', 'put', 'delete'],
    model: function model(name, Schema) {
      var _this = this;

      this.modelSchema = _mongoose2.default.model(name, Schema);
      this.modelName = name;
      this.modelSchema.register = function (app, url, middleware) {
        var aux = methods(_this.modelSchema, _this.methodsBefore, _this.methodsAfter, _this.methodsCustom, _this.routeId, middleware);
        var list = _this.modelSchema.methods || _this.methods;
        list.forEach(function (method) {
          var auxUrl = url;
          if (method === 'getOne' || method === 'put' || method === 'delete') {
            auxUrl = auxUrl + '/:_id';
          }
          app[method === 'getOne' ? 'get' : method](auxUrl, aux[method]);
        });
        _this.extraRoutes.forEach(function (route) {
          var method = route.method;
          var auxUrl = url;
          if (method === 'getOne' || method === 'put' || method === 'delete') {
            auxUrl = auxUrl + '/:_id';
          }
          app[method === 'getOne' ? 'get' : method](auxUrl + '/' + route.additionalPath, route.cb);
        });
      };
      this.modelSchema.before = function (method, cb) {
        _this.methodsBefore[method] = cb;
      };
      this.modelSchema.after = function (method, cb) {
        _this.methodsAfter[method] = cb;
      };
      this.modelSchema.route = function (additionalPath, method, cb) {
        if (!additionalPath) _this.methodsCustom[method] = cb;else _this.extraRoutes.push({ method: method, cb: cb, additionalPath: additionalPath });
      };
      return this.modelSchema;
    },
    mongoose: _mongoose2.default
  };
};