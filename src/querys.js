const mongoose = require('mongoose')

export default (Model) => ({
  get: function (req) {
    return this.prepareAggregate(req)
  },
  getOne: function (req) {
    return new Promise((resolve, reject) => {
      this.prepareAggregate(req)
        .then(data => {
          if (data.length === 0) {
            resolve(null)
          } else if (data.length === 1) {
            resolve(data[0])
          } else reject(new Error('Mais de um item retornado na busca unica'))
        })
        .catch(reject)
    })
  },
  post: (req) => {
    return Model.create(req.body)
  },
  put: (req, command = '$set') => {
    return Model.findOneAndUpdate(req.params, {[command]: {...req.body, changedAt: new Date()}}, { runValidators: true })
  },
  delete: (req) => {
    return Model.findOneAndUpdate(req.params, {$set: {disabledAt: new Date(), active: false}}, { runValidators: true })
  },
  prepareAggregate: function (req) {
    let {limit, populate, sort, select, page} = req.query
    let query = req.query
    if (query) ['limit', 'populate', 'sort', 'select', 'page'].map(i => { delete query[i] })
    const params = req.params
    let subQuerys = {}
    let listPopulate = []
    if (populate) {
      if (!(populate instanceof Array)) populate = populate.split(',')
      populate.map(field => {
        Object.keys(query).forEach(key => {
          if (key.includes(field) && key.includes('.')) {
            subQuerys[key] = query[key]
            delete query[key]
          }
        })
        this.prepareLookup(listPopulate, field)
      })
    }
    query = {...query, ...params}
    this.prepareQuery(query)
    query = [
      {
        $match: query
      }
    ]

    listPopulate.forEach(populate => query.push(populate))

    query.push({
      $match: subQuerys
    })

    if (sort) {
      const sortAux = {}
      sort.split(' ').forEach(item => {
        sortAux[item.replace('-', '')] = item.indexOf('-') > -1 ? -1 : 1
      })
      query.push({
        $sort: sortAux
      })
    }

    if (select) {
      let project = {}
      if (!(select instanceof Array)) select = select.split(',')
      select.forEach(field => {
        project[field] = 1
      })
      query.push({
        $project: project
      })
    }

    if (page) {
      query.push({
        $skip: Number(page * limit)
      })
    }
    if (limit) {
      query.push({
        $limit: Number(limit)
      })
    }

    if (process.env.debugQuery) console.log(JSON.stringify(query))

    return Model.aggregate(query)
  },
  prepareQuery: (query) => {
    Object.keys(query).forEach(key => {
      if (Model.schema.paths[key]) {
        if (Model.schema.paths[key].instance === 'Number') query[key] = Number(query[key])
        if (Model.schema.paths[key].instance === 'Date' && query[key]) console.log('cria Renge')
        if ((Model.schema.paths[key].instance === 'ObjectID' && query[key])) query[key] = mongoose.Types.ObjectId(query[key])
        if (Model.schema.paths[key].instance === 'Boolean') query[key] = query[key] === 'true'
      }
    })
  },
  prepareLookup: (list, field) => {
    let ref
    let type
    let unwind = false
    let firstField
    let split = field.split('.')
    if (field.indexOf('.') > -1 && Model.schema.paths[split[0]]) {
      firstField = split[0]
      let refChild = Model.schema.paths[split[0]].options.ref
      if (refChild) {
        ref = mongoose.model(refChild).schema.paths[split[1]].options.ref.toLowerCase()
        type = mongoose.model(refChild).schema.paths[split[1]].instance
      } else {
        let aux = Model.schema.paths[split[0]].schema.paths[split[1]]
        ref = aux.options.ref.toLowerCase()
        type = aux.instance
        unwind = true
        list.push({
          $unwind: {path: `$${split[0]}`, preserveNullAndEmptyArrays: true}
        })
      }
    }
    ref = !ref ? Model.schema.paths[field].options.ref.toLowerCase() : ref
    type = !type ? Model.schema.paths[field].instance : type
    list.push({
      $lookup: {
        from: ref,
        localField: field,
        foreignField: '_id',
        as: field
      }
    })
    if (type === 'ObjectID') {
      list.push({
        $unwind: {
          path: `$${field}`,
          preserveNullAndEmptyArrays: true
        }
      })
    }
    if (unwind) {
      let group = {}
      Object.keys(Model.schema.paths).map(k => {
        if (k !== '_id') {
          if (k.includes('.')) k = k.substring(0, k.indexOf('.'))
          group[k] = {$first: `$${k}`}
        }
      })
      // group[firstField] = {$push: `$${firstField}`}
      group[firstField] = {$push: {
        $cond: {
          if: `$${firstField}._id`,
          then: `$${firstField}`,
          else: null
        }}
      }
      list.push({
        $group: {
          '_id': '$_id',
          ...group
        }
      })
      let project = {}
      Object.keys(Model.schema.paths).map(k => { project[k] = 1 })
      project[firstField] = {
        $filter: {
          input: `$${firstField}`,
          as: 'itemGroup',
          cond: { $and: [
            '$$itemGroup._id'
          ] }
        }
      }
      list.push({
        $project: {
          _id: 1,
          ...project
        }
      })
    }
  }
})
