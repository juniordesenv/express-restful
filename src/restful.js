import mongoose from 'mongoose'
import querys from './querys'

const methods = (Model, methodsBefore, methodsAfter, methodsCustom, routeId, middleware) => {
  const querysModel = querys(Model)
  const custom = { querysModel, methodsAfter, methodsBefore, methodsCustom, middleware, routeId }
  return {
    get: (req, res, next) => {
      executeCompletProcess('get', req, res, next, custom)
    },
    getOne: (req, res, next) => {
      executeCompletProcess('getOne', req, res, next, custom)
    },
    post: (req, res, next) => {
      executeCompletProcess('post', req, res, next, custom)
    },
    put: (req, res, next) => {
      executeCompletProcess('put', req, res, next, custom)
    },
    delete: (req, res, next) => {
      executeCompletProcess('delete', req, res, next, custom)
    }
  }
}

const executeCompletProcess = async (method, req, res, next, custom) => {
  const {methodsBefore, methodsCustom, middleware, routeId} = custom
  if (middleware) {
    await new Promise((resolve) => {
      middleware(req, res, resolve, routeId)
    })
  }
  if (methodsCustom[method]) return methodsCustom[method](req, res, next)
  if (methodsBefore[method]) {
    return methodsBefore[method](req, res, () => {
      executeQuery(method, req, res, custom)
    })
  }
  executeQuery(method, req, res, custom)
}

const executeQuery = (method, req, res, custom) => {
  const {querysModel, methodsAfter} = custom
  const after = methodsAfter[method]
  querysModel[method](req)
    .then(data => {
      if (!after) return res.send(data)
      res.locals.data = data
      after(req, res, () => {
        res.send(res.locals.data)
      })
    })
    .catch(err => {
      console.log(err)
      let errors = err.errors || [{message: err.message}]
      if (!Array.isArray(errors)) {
        errors = Object.keys(errors).map(i => errors[i])
      }
      errors = errors.map(e => e.message)
      if (!after) return res.status(422).send({errors})
      res.locals.errors = errors
      after(req, res, () => {
        res.status(422).send({errors})
      })
    })
}

export default () => ({
  methodsBefore: {},
  methodsAfter: {},
  methodsCustom: {},
  extraRoutes: [],
  modelSchema: null,
  modelName: null,
  routeId: 0,
  methods: ['get', 'getOne', 'post', 'put', 'delete'],
  model: function (name, Schema) {
    this.modelSchema = mongoose.model(name, Schema)
    this.modelName = name
    this.modelSchema.register = (app, url, middleware) => {
      const aux = methods(this.modelSchema, this.methodsBefore, this.methodsAfter, this.methodsCustom, this.routeId, middleware)
      let list = this.modelSchema.methods || this.methods
      list.forEach(method => {
        let auxUrl = url
        if (method === 'getOne' || method === 'put' || method === 'delete') {
          auxUrl = `${auxUrl}/:_id`
        }
        app[method === 'getOne' ? 'get' : method](auxUrl, aux[method])
      })
      this.extraRoutes.forEach(route => {
        let method = route.method
        let auxUrl = url
        if (method === 'getOne' || method === 'put' || method === 'delete') {
          auxUrl = `${auxUrl}/:_id`
        }
        app[method === 'getOne' ? 'get' : method](`${auxUrl}/${route.additionalPath}`, route.cb)
      })
    }
    this.modelSchema.before = (method, cb) => {
      this.methodsBefore[method] = cb
    }
    this.modelSchema.after = (method, cb) => {
      this.methodsAfter[method] = cb
    }
    this.modelSchema.route = (additionalPath, method, cb) => {
      if (!additionalPath) this.methodsCustom[method] = cb
      else this.extraRoutes.push({method, cb, additionalPath})
    }
    return this.modelSchema
  },
  mongoose
})
